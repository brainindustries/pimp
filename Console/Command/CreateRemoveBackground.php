<?php

namespace Shirtplatform\Pimp\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State;
use Shirtplatform\Core\Model\ProductMaker;
use Shirtplatform\Pimp\Helper\Data;

class CreateRemoveBackground extends Command
{
    /**
     * @var State
     */
    private $_state;

    /**
     * @var ProductMaker
     */
    private $_productMaker;

    /**
     * @var Data
     */
    private $_helper;

    /**
     * @param State $state
     * @param ProductMaker $productMaker
     * @param Data $helper
     * @param string $name
     */
    public function __construct(
        State $state,
        ProductMaker $productMaker,
        Data $helper,
        $name = null
    ) {
        parent::__construct($name);
        $this->_state = $state;
        $this->_productMaker = $productMaker;
        $this->_helper = $helper;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('shirtplatform:create-product:remove-background');
        $this->setDescription('Creates Remove Background product.');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
        try {
            $removeBackgroundData = $this->_helper->getRemoveBackgroundData();
            $product = $this->_productMaker->createProduct($removeBackgroundData);
            $output->writeln('<info>' . $product->getName() . ' was successfully created.</info>');
            return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
        } catch (\Exception $ex) {
            $output->writeln('<error>' . $ex->getMessage() . '</error>');
            return \Magento\Framework\Console\Cli::RETURN_FAILURE;
        }
    }
}