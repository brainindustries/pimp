<?php

namespace Shirtplatform\Pimp\Helper;

use shirtplatform\entity\order\Order;
use Magento\Eav\Api\AttributeSetRepositoryInterface;
use Shirtplatform\Core\Helper\Data as CoreHelper;
use Magento\Catalog\Helper\Product\Configuration;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Backend\Model\Session\Quote;
use Magento\Framework\App\Helper\Context;
use Magento\Eav\Model\Entity\Attribute\Set;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Framework\Webapi\ServiceInputProcessor;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Module\Dir\Reader;
use Shirtplatform\Core\Model\ProductMaker;

class Data extends \Magento\Framework\App\Helper\AbstractHelper 
{
    const REMOVE_BACKGROUND_PRODUCT_SKU = 'RB01';        

    const PIMPSERVICE_ATTRIBUTESET_NAME = 'PIMP Service';

    /**
     * @var AttributeSetRepositoryInterface
     */
    private $_attributeSetRepository;

    /**
     * @var CoreHelper
     */
    private $_coreHelper;

    /**
     * @var Set
     */
    private $_pimpAttributeSet;

    /**
     * @var Configuration
     */
    private $_productConfig;

    /**
     * @var SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * @var Quote
     */
    private $_sessionQuote;

    /**
     * @var ProductResource
     */
    private $_productResource;

    /**
     * @var ServiceInputProcessor
     */
    private $_serviceInputProcessor;

    /**
     * @var File
     */
    private $_ioFile;

    /**
     * @var Reader
     */
    private $_dirReader;

    /**
     * @var ProductMaker
     */
    private $_productMaker;

    /**
     * 
     * @param AttributeSetRepositoryInterface $attributeSetRepository
     * @param CoreHelper $coreHelper
     * @param Configuration $productConfig
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param Quote $sessionQuote
     * @param Context $context
     * @param ProductResource $productResource
     * @param ServiceInputProcessor $serviceInputProcessor
     * @param File $ioFile
     * @param Reader $dirReader
     * @param ProductMaker $productMaker
     */
    public function __construct(
        AttributeSetRepositoryInterface $attributeSetRepository,
        CoreHelper $coreHelper,
        Configuration $productConfig,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Quote $sessionQuote,
        Context $context,
        ProductResource $productResource,
        ServiceInputProcessor $serviceInputProcessor,
        File $ioFile,
        Reader $dirReader,
        ProductMaker $productMaker
    ) {
        parent::__construct($context);
        $this->_attributeSetRepository = $attributeSetRepository;
        $this->_coreHelper = $coreHelper;
        $this->_productConfig = $productConfig;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_sessionQuote = $sessionQuote;
        $this->_productResource = $productResource;
        $this->_serviceInputProcessor = $serviceInputProcessor;
        $this->_ioFile = $ioFile;
        $this->_dirReader = $dirReader;
        $this->_productMaker = $productMaker;

        $searchCriteria = $this->_searchCriteriaBuilder->addFilter('attribute_set_name', 'PIMP Service')->create();
        $attributeSetList = $this->_attributeSetRepository->getList($searchCriteria)->getItems();
        $pimpAttributeSet = reset($attributeSetList);

        if ($pimpAttributeSet) {
            $this->_pimpAttributeSet = $pimpAttributeSet;
        }
    }

    /**
     * Is the item pimp service item?
     * 
     * @access public
     * @param \Magento\Quote\Model\Quote\Item|\Magento\Sales\Model\Order\Item $item
     * @return boolean
     */
    public function isPimpServiceItem($item) {
        $customOptions = [];

        if ($item instanceof \Magento\Quote\Model\Quote\Item) {
            $customOptions = $this->_productConfig->getOptions($item);
        }
        else {
            $options = $item->getProductOptions();

            if (isset($options['options'])) {
                $customOptions = $options['options'];
            }
        }

        foreach ($customOptions as $option) {
            if ($option['label'] == 'pimp_task_id') {
                return true;
            }
        }

        return false;
    }

    /**
     * Is the product pimp service product?
     * 
     * @access public
     * @param \Magento\Catalog\Model\Product $product
     * @return boolean
     */
    public function isPimpServiceProduct($product) {
        return $this->_pimpAttributeSet and $product->getAttributeSetId() == $this->_pimpAttributeSet->getAttributeSetId();
    }

    /**
     * Ignore all pimp service items in order and save it to session
     * 
     * @access public
     * @param \Magento\Sales\Model\Order $order
     */
    public function ignoreAllPimpServiceItemsInOrder($order) {        
        if ($order->getShirtplatformId()) {
            $this->_coreHelper->shirtplatformAuth($order->getStoreId());
            $platformOrder = Order::find($order->getShirtplatformId());
            $manualServices = $platformOrder->getOrderedManualServices();
            $ignorePimpServiceItems = $this->_sessionQuote->getIgnorePimpServiceItems();

            if ($ignorePimpServiceItems == null) {
                $ignorePimpServiceItems = [];
            }

            if ($manualServices) {
                foreach ($manualServices as $service) {
                    $ignorePimpServiceItems[$service->pimpOutputActionId] = $service->pimpOutputActionId;
                }
            }

            $this->_sessionQuote->setIgnorePimpServiceItems($ignorePimpServiceItems);
        }
    }

    /**
     * @access public
     * @return string|null
     */
    public function getPimpServiceCode()
    {
        $attribute = $this->_productResource->getAttribute('pimp_service_code');
        foreach ($attribute->getOptions() as $_option) {
            if ($_option->getLabel() == 'Remove Background') {
                return $_option->getValue();
            }
        }
        return null;
    }

    /**
     * @return array
     */
    public function getRemoveBackgroundMediaGalleryEntry()
    {
        $viewDir = $this->_dirReader->getModuleDir(\Magento\Framework\Module\Dir::MODULE_VIEW_DIR, 'Shirtplatform_Pimp');
        $srcImgPath = $viewDir . '/base/web/img/RB-MagentoIcon.png';
        $mediaGallery = [
            'media_type' => 'image',
            'label' => 'Main image',
            'position' => 0,
            'disabled' => false,
            'types' => ['image', 'small_image', 'thumbnail'],
            'content' => [
                'base64_encoded_data' => base64_encode($this->_ioFile->read($srcImgPath)),
                'type' => 'image/png',
                'name' => 'RB-MagentoIcon.png'
            ]
        ];
        $entry = $this->_serviceInputProcessor->convertValue($mediaGallery, \Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface::class);
        return $entry;
    }

    /**
     * @return array
     */
    public function getRemoveBackgroundData()
    {
        return [
            'product_data' => [
                'sku' => \Shirtplatform\Pimp\Helper\Data::REMOVE_BACKGROUND_PRODUCT_SKU,
                'name' => 'Remove Background',
                'attribute_set_id' => $this->_productMaker->getAttributeSetId(\Shirtplatform\Pimp\Helper\Data::PIMPSERVICE_ATTRIBUTESET_NAME),
                'visibility' => \Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE,
                'type_id' => \Magento\Catalog\Model\Product\Type::TYPE_VIRTUAL,
                'pimp_service_code' => $this->getPimpServiceCode(),
            ],
            'custom_options' => [
                [
                    'title' => 'pimp_task_id',
                    'type' => 'field',
                    'is_require' => true,
                    'sort_order' => 1,
                    'price' => 0,
                    'price_type' => 'fixed',
                    'sku' => 'RBTASKID',
                    'max_characters' => 0,
                ]
            ],
            'media_gallery_entries' => [
                $this->getRemoveBackgroundMediaGalleryEntry()
            ]
        ];
    }
}
