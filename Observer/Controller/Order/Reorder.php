<?php

namespace Shirtplatform\Pimp\Observer\Controller\Order;

use Magento\Framework\Event\ObserverInterface;

class Reorder implements ObserverInterface {
    
    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $_checkoutSession;
    
    /**
     * 
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(\Magento\Checkout\Model\Session $checkoutSession) {
        $this->_checkoutSession = $checkoutSession;
    }
    
    /**
     * Unset ignore_pimp_service_items on checkout session during reorder action
     * to make sure there are not any from some previous request
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $this->_checkoutSession->unsIgnorePimpServiceItems();
    }
}