<?php

namespace Shirtplatform\Pimp\Observer;

use Magento\Framework\Event\ObserverInterface;

class UpdateManualServices implements ObserverInterface {

    /**
     * @var \Magento\Framework\Registry
     */
    private $_coreRegistry;

    /**
     * @var \Shirtplatform\Pimp\Model\ManualService
     */
    private $_manualService;

    /**
     * 
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Shirtplatform\Pimp\Model\ManualService $manualService
     */
    public function __construct(\Magento\Framework\Registry $coreRegistry,
                                \Shirtplatform\Pimp\Model\ManualService $manualService) {
        $this->_coreRegistry = $coreRegistry;
        $this->_manualService = $manualService;
    }

    /**
     * Update manual services in cart - add/remove magento service products
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $updateManualServices = $this->_coreRegistry->registry('updateManualServices');
        
        if ($updateManualServices) {
            $quote = $observer->getQuote();            
            $this->_manualService->updateManualServices($quote);
            $this->_coreRegistry->unregister('updateManualServices');
        }
    }

}
