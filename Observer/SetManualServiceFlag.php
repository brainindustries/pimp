<?php

namespace Shirtplatform\Pimp\Observer;

use Magento\Framework\Event\ObserverInterface;

class SetManualServiceFlag implements ObserverInterface {
    
    /**     
     * @var \Magento\Core\Registry
     */
    private $_coreRegistry;
    
    /**     
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(\Magento\Framework\Registry $coreRegistry) {
        $this->_coreRegistry = $coreRegistry;
    }
    
    /**
     * Set updateManualServices flag on true
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $this->_coreRegistry->register('updateManualServices', true, true);
    }
}