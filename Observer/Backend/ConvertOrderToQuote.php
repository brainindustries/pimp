<?php

namespace Shirtplatform\Pimp\Observer\Backend;

use Magento\Framework\Event\ObserverInterface;

class ConvertOrderToQuote implements ObserverInterface {
    
    /**
     * @var \Magento\Framework\Registry
     */
    private $_coreRegistry;
    
    /**
     * 
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(\Magento\Framework\Registry $coreRegistry) {
        $this->_coreRegistry = $coreRegistry;
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $this->_coreRegistry->register('updateManualServices', true, true);
    }
}