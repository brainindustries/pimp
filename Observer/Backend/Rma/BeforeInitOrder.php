<?php

namespace Shirtplatform\Pimp\Observer\Backend\Rma;

use Magento\Framework\Event\ObserverInterface;

class BeforeInitOrder implements ObserverInterface {
    
    /**     
     * @var \Shirtplatform\Pimp\Helper\Data
     */
    private $_pimpHelper;
    
    /**
     * 
     * @param \Shirtplatform\Pimp\Helper\Data $pimpHelper
     */
    public function __construct(\Shirtplatform\Pimp\Helper\Data $pimpHelper) {
        $this->_pimpHelper = $pimpHelper;
    }
    
    /**
     * Ignore all pimp service items in exchange order and save to session
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $order = $observer->getOrder();
        $this->_pimpHelper->ignoreAllPimpServiceItemsInOrder($order);
    }
}