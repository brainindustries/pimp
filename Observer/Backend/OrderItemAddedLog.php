<?php

namespace Shirtplatform\Pimp\Observer\Backend;

use Magento\Framework\Event\ObserverInterface;

class OrderItemAddedLog implements ObserverInterface {

    /**
     * @var \Shirtplatform\Pimp\Helper\Data
     */
    private $_pimpHelper;

    /**
     * 
     * @param \Shirtplatform\Pimp\Helper\Data $pimpHelper
     */
    public function __construct(\Shirtplatform\Pimp\Helper\Data $pimpHelper) {
        $this->_pimpHelper = $pimpHelper;
    }

    /**
     * Compare pimp items and updates comment. Pimp items are not initialized during
     * initFromOrder() and are always added in updateManualService observer. That's
     * why we need to compare the products here.
     * 
     * @access public
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        $fieldMapper = [
            'price' => __('Price'),
            'original_price' => __('Original Price'),
            'tax_percent' => __('Tax Percent'),
            'tax_amount' => __('Tax Amount'),
            'discount_percent' => __('Discount Percent'),
            'discount_amount' => __('Discount Amount'),
            'row_total' => __('Row Total')
        ];

        $orderItem = $observer->getOrderItem();
        $orderItemOptions = $observer->getOrderItemOptions();
        $oldOrderData = $observer->getOldOrderData();
        $transport = $observer->getTransport();

        if ($this->_pimpHelper->isPimpServiceItem($orderItem) and isset($orderItemOptions['options'])) {
            foreach ($orderItemOptions['options'] as $option) {
                if ($option['label'] == 'pimp_task_id') {
                    $oldOrderItem = $this->_findOldPimpServiceItem($oldOrderData, $option['value']);                    
                    if ($oldOrderItem) {                        
                        $newComment = '';

                        foreach ($fieldMapper as $field => $value) {
                            $oldValue = $oldOrderItem[$field];
                            $newValue = $orderItem->getData($field);

                            if ($this->_isDecimalField('order_item', $field)) {
                                $oldValue = number_format($oldValue, 4, '.', '');
                                $newValue = number_format($newValue, 4, '.', '');
                            }

                            if ($oldValue != $newValue) {
                                if (empty($newComment)) {
                                    $newComment .= '<b>Order item "' . $oldOrderItem['name'] . '", SKU: ' . $oldOrderItem['sku'] . " has been updated with data:</b><br>\n";
                                }
                                
                                $newComment .= $fieldMapper[$field] . ' has been updated from ' . $oldValue . ' to ' . $newValue . "<br>\n";
                            }
                        }                                                
                        
                        $transport->setItemComment($newComment);
                    }
                }
            }                        
        }        
    }

    /**
     * Find old order item with the given pimp_task_id
     * 
     * @access private
     * @param array $oldOrderData
     * @param int $taskId pimp_task_id of the old order item
     * @return array|null
     */
    private function _findOldPimpServiceItem($oldOrderData,
                                             $taskId) {
        foreach ($oldOrderData['order_item'] as $oldItem) {
            if (isset($oldItem['options']['options'])) {
                foreach ($oldItem['options']['options'] as $option) {
                    if ($option['label'] == 'pimp_task_id' and $option['value'] == $taskId) {
                        return $oldItem;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Is field a decimal type?
     * 
     * @access private     
     * @param string $field
     * @return boolean
     */
    private function _isDecimalField($field) {
        $decimalFields = [
            'qty_ordered', 'price', 'original_price', 'tax_percent', 'tax_amount', 'discount_percent', 'discount_amount', 'row_total'
        ];

        return in_array($field, $decimalFields);
    }

}
