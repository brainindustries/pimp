<?php

namespace Shirtplatform\Pimp\CustomerData;

use Magento\Catalog\Helper\Image;
use Magento\Msrp\Helper\Data as MsrpHelper;
use Magento\Framework\UrlInterface;
use Magento\Catalog\Helper\Product\ConfigurationPool;
use Magento\Checkout\Helper\Data as CheckoutHelper;
use Magento\Framework\Escaper;
use Magento\Catalog\Model\Product\Configuration\Item\ItemResolverInterface;
use Magento\Framework\Serialize\SerializerInterface;

class PimpItem extends \Magento\Checkout\CustomerData\DefaultItem {

    /**
     * @var SerializerInterface
     */
    private $_serializer;

    public function __construct(
        Image $imageHelper,
        MsrpHelper $msrpHelper,
        UrlInterface $urlBuilder,
        ConfigurationPool $configurationPool,
        CheckoutHelper $checkoutHelper,
        SerializerInterface $serializer,
        Escaper $escaper = null,
        ItemResolverInterface $itemResolver = null
    ) {
        parent::__construct($imageHelper, $msrpHelper, $urlBuilder, $configurationPool, $checkoutHelper, $escaper, $itemResolver);
        $this->_serializer = $serializer;
    }

    /**
     * Get quote item data for minicart. Add pimp_product product type.
     * 
     * @access protected
     * @return array
     */
    protected function doGetItemData() {
        $itemData = parent::doGetItemData();
        $itemData['product_type'] = 'pimp_product';        
        $itemData['product_has_url'] = false;

        // if (is_array($itemData['options'])){
        //     $itemData['options'] = $this->_serializer->serialize($itemData['options']);
        // }

        return $itemData;
    }

}
