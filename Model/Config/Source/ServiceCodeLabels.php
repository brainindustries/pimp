<?php

namespace Shirtplatform\Pimp\Model\Config\Source;

class ServiceCodeLabels implements \Magento\Framework\Option\ArrayInterface {

    /**
     * PIPM service code to label mapper
     * 
     * @access public
     * @var array
     */
    public static $codeLabels = [
        'PIMP_REMOVE_BACKGROUND' => 'Remove Background'
    ];
    
    /**
     * Options getter
     *
     * @access public
     * @return array
     */
    public function toOptionArray() {
        $options = [];
        
        foreach (self::$_codeLabels as $key => $value) {
            $options[] = ['value' => $key, 'label' => $value];
        }
        
        return $options;
    }

}
