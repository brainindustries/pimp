<?php

namespace Shirtplatform\Pimp\Model;

use shirtplatform\entity\order\OrderedManualService;
use Shirtplatform\Pimp\Model\Config\Source\ServiceCodeLabels;

class ManualService {

    /**
     * Local storage of manual services for order during one HTTP request,
     * so that we save time and don't have to request every time after a product
     * is added to cart
     * 
     * @access private
     * @var array
     */
    private $_manualServices = [];

    /**
     * Pimp product collection
     *
     * @access private 
     * @var array
     */
    private $_pimpProducts = null;

    /**
     * @var \Magento\Framework\App\State
     */
    private $_appState;

    /**
     *
     * @var \Magento\Eav\Api\AttributeSetRepositoryInterface
     */
    private $_attributeSetRepository;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $_checkoutSession;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    private $_eventManager;

    /**
     * @var \Magento\Catalog\Helper\Product\Configuration
     */
    private $_productConfig = null;

    /**
     *
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $_productRepository;

    /** 
     * @var \Magento\Framework\App\Config\ScopeConfigInterface 
     */
    private $_scopeConfig;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $_searchCriteriaBuilder;

    /**
     * @var \Magento\Backend\Model\Session\Quote
     */
    private $_sessionQuote;

    /**
     * @var \Shirtplatform\Core\Helper\Data
     */
    private $_shirtplatformCoreHelper;

    /**
     * @var \Magento\Tax\Api\TaxCalculationInterface
     */
    private $_taxCalculation;

    /**
     * 
     * @param \Magento\Framework\App\State $appState
     * @param \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSetRepository
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Catalog\Helper\Product\Configuration $productConfig
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Backend\Model\Session\Quote $sessionQuote
     * @param \Shirtplatform\Core\Helper\Data $shirtplatformCoreHelper
     * @param \Magento\Tax\Api\TaxCalculationInterface $taxCalculation
     */
    public function __construct(\Magento\Framework\App\State $appState,
                                \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSetRepository,
                                \Magento\Checkout\Model\Session $checkoutSession,
                                \Magento\Framework\Event\ManagerInterface $eventManager,
                                \Magento\Catalog\Helper\Product\Configuration $productConfig,
                                \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
                                \Magento\Backend\Model\Session\Quote $sessionQuote,
                                \Shirtplatform\Core\Helper\Data $shirtplatformCoreHelper,
                                \Magento\Tax\Api\TaxCalculationInterface $taxCalculation) {
        $this->_attributeSetRepository = $attributeSetRepository;
        $this->_appState = $appState;
        $this->_checkoutSession = $checkoutSession;
        $this->_eventManager = $eventManager;
        $this->_productConfig = $productConfig;
        $this->_productRepository = $productRepository;
        $this->_scopeConfig = $scopeConfig;
        $this->_searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_sessionQuote = $sessionQuote;
        $this->_shirtplatformCoreHelper = $shirtplatformCoreHelper;
        $this->_taxCalculation = $taxCalculation;
    }
    
    /**
     * Update manual services in cart
     * 
     * @access public
     * @param \Magento\Quote\Model\Quote $quote
     * @param float $customPrice
     * @return void
     */
    public function updateManualServices($quote,
                                         $customPrice = null) {
        $platformOrderIds = [];
        $doApiRequest = false;

        foreach ($quote->getAllItems() as $item) {
            if ($item->getShirtplatformOrigOrderId() and ! in_array($item->getShirtplatformOrigOrderId(), $platformOrderIds)) {
                $platformOrderIds[] = $item->getShirtplatformOrigOrderId();
            }
        }

        if (!empty($platformOrderIds)) {
            $this->_shirtplatformCoreHelper->shirtplatformAuth($quote->getStoreId());

            foreach ($platformOrderIds as $orderId) {
                if (!isset($this->_manualServices[$orderId])) {
                    $doApiRequest = true;
                    $this->_manualServices[$orderId] = &OrderedManualService::findAll([$orderId], null, null, true);
                }
            }

            if ($doApiRequest) {
                \shirtplatform\rest\REST::getInstance()->wait();
            }
        }
        
        $servicesInCart = $this->_getServiceItemsInCart($quote);

        if (!empty($this->_manualServices)) {                                      
            if ($this->_appState->getAreaCode() == 'adminhtml') {
                $ignorePimpServiceItems = $this->_sessionQuote->getIgnorePimpServiceItems();
            }
            else {
                $ignorePimpServiceItems = $this->_checkoutSession->getIgnorePimpServiceItems();
            }
            
            if ($ignorePimpServiceItems == null) {
                $ignorePimpServiceItems = [];
            }
            
            //get pimp products
            $pimpProducts = $this->_getPimpServiceProducts();

            if (count($pimpProducts) == 0) {
                return;
            }

            //get tax percent
            $pimpVAT = $this->_getPimpVAT($quote);

            //ignore pimp products during reorder
            $ignorePimpProducts = (int)$this->_scopeConfig->getValue(
                'sales/reorder/ignore_pimp_products_during_reorder', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $quote->getStoreId()
            );

            $this->_removeSameTasksForDifferentOrders();

            foreach ($this->_manualServices as $orderId => $services) {
                foreach ($services as $_service) {
                    $serviceLabel = null;
                    if (isset(ServiceCodeLabels::$codeLabels[$_service->code])) {
                        $serviceLabel = ServiceCodeLabels::$codeLabels[$_service->code];
                    }
                    if ($serviceLabel == null) {
                        continue;
                    }

                    //product has to be cloned when initializing quote from order in admin to make sure quote items are 
                    //added separately and not only one item with modified quantity
                    $product = clone $this->_getPimpProductForCode($serviceLabel);
                    if ($product == null) {
                        continue;
                    }

                    $taskOptionId = $this->_getPimpTaskId($product);
                    if ($taskOptionId == null) {
                        continue;
                    }

                    if (!isset($servicesInCart[$_service->pimpOutputActionId]) && (!$ignorePimpProducts || !in_array($_service->pimpOutputActionId, $ignorePimpServiceItems))) {                                            
                        $params = [
                            'qty' => $_service->amount,
                            'options' => [
                                $taskOptionId => $_service->pimpOutputActionId
                            ]
                        ];

                        $request = new \Magento\Framework\DataObject($params);
                        $newItem = $quote->addProduct($product, $request); 
                        $newItem->setQty($_service->amount);                           

                        //set custom_price: $service->price + VAT
                        if ($newItem instanceof \Magento\Quote\Model\Quote\Item) {
                            if ($customPrice !== null) {
                                $newItem->setOriginalCustomPrice($customPrice);
                                $newItem->setCustomPrice($customPrice);
                            }
                            else {
                                //should be tested for various settings (price incl/excl tax) in admin
//                                    $taxPercent = $pimpVAT[$product->getId()];
//                                    $priceInclVat = $_service->price + ($taxPercent / 100 * $_service->price);
//                                    $priceInclVat = round($priceInclVat, 2);
//                                    $newItem->setOriginalCustomPrice($priceInclVat);
//                                    $newItem->setCustomPrice($priceInclVat);
                                $newItem->setOriginalCustomPrice($_service->price);
                                $newItem->setCustomPrice($_service->price);
                            }
                        }

                        $this->_eventManager->dispatch(
                            'shirtplatform_pimp_manual_service_add',
                            ['items' => [$newItem]]
                        );                        
                    }

                    //unset this service as we will delete what's left in this array
                    unset($servicesInCart[$_service->pimpOutputActionId]);
                }
            }
        }

        //remove services from cart, because they don't exist in platform order any more
        if ($servicesInCart) {
            foreach ($servicesInCart as $item) {
                $quote->removeItem($item->getId());
            }
        }
    }

    /**
     * Get pimp service products
     * 
     * @access private
     * @return array of products
     */
    private function _getPimpServiceProducts() {
        if (is_null($this->_pimpProducts)) {
            $this->_pimpProducts = array();
            $searchCriteria = $this->_searchCriteriaBuilder->addFilter('attribute_set_name', 'PIMP Service')->create();
            $attributeSetList = $this->_attributeSetRepository->getList($searchCriteria)->getItems();
            $pimpAttributeSet = reset($attributeSetList);

            if (empty($pimpAttributeSet)) {
                return $this->_pimpProducts;
            }

            $searchCriteria = $this->_searchCriteriaBuilder
                    ->addFilter('attribute_set_id', $pimpAttributeSet->getAttributeSetId())
                    ->create();
            $searchResult = $this->_productRepository->getList($searchCriteria)->getItems();

            //we have to reload the product, so that they'll have custom options loaded
            foreach ($searchResult as $res) {
                $this->_pimpProducts[] = $this->_productRepository->getById($res->getId(), false, null, true);
            }
        }

        return $this->_pimpProducts;
    }

    /**
     * Get product with specified pimp service code label
     * 
     * @access private
     * @param string $pimpServiceOptionLabel
     * @return \Magento\Catalog\Model\Product|null
     */
    private function _getPimpProductForCode($pimpServiceOptionLabel) {
        $pimpProducts = $this->_getPimpServiceProducts();

        foreach ($pimpProducts as $product) {
            $options = $product->getResource()->getAttribute('pimp_service_code')->getSource()->getAllOptions(false, true);

            foreach ($options as $_option) {
                if ($_option['label'] == $pimpServiceOptionLabel) {
                    return $product;
                }
            }
        }

        return null;
    }

    /**
     * Get array mapping productId to its VAT (in percents)
     * 
     * @access private
     * @param \Magento\Quote\Model\Quote $quote
     * @return array
     */
    private function _getPimpVAT($quote = null) {
        $result = [];
        $customerId = null;
        $storeId = null;

        if ($quote) {
            $customerId = $quote->getCustomerId();
            $storeId = $quote->getStoreId();
        }

        $pimpProducts = $this->_getPimpServiceProducts();
        foreach ($pimpProducts as $product) {
            $taxClassId = $product->getTaxClassId();
            $vat = $this->_taxCalculation->getCalculatedRate($taxClassId, $customerId, $storeId);
            $result[$product->getId()] = $vat;
        }

        return $result;
    }

    /**
     * Get option id of product custom option with pimp_task_id title
     * 
     * @access private
     * @param \Magento\Catalog\Model\Product $product
     * @return int|null
     */
    private function _getPimpTaskId($product) {
        $options = $product->getProductOptionsCollection();
        foreach ($options as $opt) {
            if ($opt->getTitle() == 'pimp_task_id') {
                return $opt->getOptionId();
            }
        }

        return null;
    }
    
    /**
     * Remove manual tasks with the same pimpOutputAction that belong to different orders.
     * 
     * This happens when a customer reloads the creator page. The items belong to different platform
     * orders and some of these orders might have the same id. It would cause adding the pimp
     * product to the magento cart multiple times
     *
     * @access private
     * @return void
     */
    private function _removeSameTasksForDifferentOrders()
    {
        $pimpTaskIds = [];

        foreach ($this->_manualServices as $orderId => $services) {
            foreach ($services as $serviceId => $_service) {
                if (in_array($_service->pimpOutputActionId, $pimpTaskIds)) {
                    unset($this->_manualServices[$orderId][$serviceId]);
                }
                $pimpTaskIds[] = $_service->pimpOutputActionId;
            }
        }
    }

    /**
     * Get service items that are already in cart
     * 
     * @access private
     * @param \Magento\Quote\Model\Quote $quote
     * @return array
     */
    public function _getServiceItemsInCart($quote) {
        $inCart = [];

        foreach ($quote->getAllItems() as $item) {
            $customOptions = $this->_productConfig->getOptions($item);

            foreach ($customOptions as $option) {
                if ($option['label'] == 'pimp_task_id') {
                    $inCart[$option['value']] = $item;
                }
            }
        }

        return $inCart;
    }

}
