<?php

namespace Shirtplatform\Pimp\Plugin\Checkout\Model;

class Cart {

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $_checkoutSession;

    /**
     * @var \Shirtplatform\Pimp\Helper\Data
     */
    private $_pimpHelper;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $_request;

    /**
     * 
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Shirtplatform\Pimp\Helper\Data $pimpHelper
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(\Magento\Checkout\Model\Session $checkoutSession,
                                \Shirtplatform\Pimp\Helper\Data $pimpHelper,
                                \Magento\Framework\App\RequestInterface $request) {
        $this->_checkoutSession = $checkoutSession;
        $this->_pimpHelper = $pimpHelper;
        $this->_request = $request;
    }

    /**
     * Don't add product to cart for pimp service items during reorder
     * 
     * @access public
     * @param \Magento\Checkout\Model\Cart $subject
     * @param \Closure $proceed
     * @param \Magento\Sales\Model\Order\Item $orderItem
     * @param true|null $qtyFlag if is null set product qty like in order
     * @return \Magento\Checkout\Model\Cart
     */
    public function aroundAddOrderItem($subject,
                                       $proceed,
                                       $orderItem,
                                       $qtyFlag = null) {
        $actionName = $this->_request->getFullActionName();

        if ($actionName == 'shirtplatform_creator_order_reorder' and $this->_pimpHelper->isPimpServiceItem($orderItem)) {
            $ignorePimpServiceItems = $this->_checkoutSession->getIgnorePimpServiceItems();
            
            $options = $orderItem->getProductOptions();
            if (isset($options['options'])) {
                foreach ($options['options'] as $_option) {
                    if ($_option['label'] == 'pimp_task_id') {
                        $ignorePimpServiceItems[] = $_option['value'];
                        break;
                    }
                }
            }

            $this->_checkoutSession->setIgnorePimpServiceItems($ignorePimpServiceItems);            
            return $subject;
        }

        return $proceed($orderItem, $qtyFlag);
    }

}
