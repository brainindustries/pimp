<?php

namespace Shirtplatform\Pimp\Plugin\Checkout\Block\Cart;

class AbstractCart {

    /**     
     * @var \Shirtplatform\Pimp\Helper\Data
     */
    private $_pimpHelper;
    
    /**
     * 
     * @param \Shirtplatform\Pimp\Helper\Data $pimpHelper
     */
    public function __construct(\Shirtplatform\Pimp\Helper\Data $pimpHelper) {
        $this->_pimpHelper = $pimpHelper;
    }
    
    /**
     * Get item row html. Use "pimp" item renderer for shirtplatform pimp products
     * 
     * @access public
     * @param \Magento\Checkout\Block\Cart\AbstractClass $subject
     * @param Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return string
     */
    public function aroundGetItemHtml($subject,
                                      $proceed,
                                      $item) {        
        if ($this->_pimpHelper->isPimpServiceProduct($item->getProduct())) {
            $renderer = $subject->getItemRenderer('pimp_product')->setItem($item);
            return $renderer->toHtml();
        }

        return $proceed($item);
    }

}
