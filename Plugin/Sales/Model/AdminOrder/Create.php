<?php

namespace Shirtplatform\Pimp\Plugin\Sales\Model\AdminOrder;

class Create {

    /**
     * @var \Shirtplatform\Pimp\Helper\Data
     */
    private $_pimpHelper;

    /**
     * @var \Magento\Backend\Model\Session\Quote
     */
    private $_sessionQuote;

    /**
     *      
     * @param \Shirtplatform\Pimp\Helper\Data $pimpHelper     
     * @param \Magento\Backend\Model\Session\Quote $sessionQuote
     */
    public function __construct(\Shirtplatform\Pimp\Helper\Data $pimpHelper,
                                \Magento\Backend\Model\Session\Quote $sessionQuote) {        
        $this->_pimpHelper = $pimpHelper;
        $this->_sessionQuote = $sessionQuote;
    }

    /**
     * Ignore all manual services
     * 
     * @access public
     * @param \Magento\Sales\Model\AdminOrder\Create $subject
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    public function beforeInitFromOrder($subject,
                                        $order) {
        $this->_pimpHelper->ignoreAllPimpServiceItemsInOrder($order);        
        return [$order];
    }

    /**
     * Don't init quote item for pimp service items, but if their pimp_task_id is 
     * found, remove their pimp_task_id from $ignorePimpServiceItems. They will be
     * added later during collectTotals observer
     * 
     * @access public
     * @param \Magento\Sales\Model\AdminOrder\Create $subject
     * @param \Closure $proceed
     * @param \Magento\Sales\Model\Order\Item $orderItem
     * @param int $qty
     * @return \Magento\Quote\Model\Quote\Item|string|\Magento\Sales\Model\AdminOrder\Create
     */
    public function aroundInitFromOrderItem($subject,
                                            $proceed,
                                            $orderItem,
                                            $qty = null) {        
        if ($this->_pimpHelper->isPimpServiceItem($orderItem)) {
            $ignorePimpServiceItems = $this->_sessionQuote->getIgnorePimpServiceItems();

            if ($ignorePimpServiceItems == null) {
                $ignorePimpServiceItems = [];
            }

            $options = $orderItem->getProductOptions();
            if (isset($options['options'])) {
                foreach ($options['options'] as $_option) {
                    if ($_option['label'] == 'pimp_task_id') {
                        unset($ignorePimpServiceItems[$_option['value']]);                        
                        break;
                    }
                }
            }

            $this->_sessionQuote->setIgnorePimpServiceItems($ignorePimpServiceItems);
            return $subject;
        }

        return $proceed($orderItem, $qty);
    }

}
