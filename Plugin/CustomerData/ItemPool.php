<?php

namespace Shirtplatform\Pimp\Plugin\CustomerData;

class ItemPool {

    /**
     * @var \Shirtplatform\Pimp\Helper\Data
     */
    private $_pimpHelper;

    /**
     * @var \Shirtplatform\Pimp\CustomerData\PimpItem
     */
    private $_pimpItem;

    /**
     * 
     * @param \Shirtplatform\Pimp\Helper\Data $pimpHelper
     * @param \Shirtplatform\Pimp\CustomerData\PimpItem $pimpItem
     */
    public function __construct(\Shirtplatform\Pimp\Helper\Data $pimpHelper,
                                \Shirtplatform\Pimp\CustomerData\PimpItem $pimpItem) {
        $this->_pimpHelper = $pimpHelper;
        $this->_pimpItem = $pimpItem;        
    }

    /**
     * Get item data for product
     * 
     * @access public
     * @param \Magento\Checkout\CustomerData\ItemPoolInterface $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return array
     */
    public function aroundGetItemData($subject,
                                      $proceed,
                                      $item) {
            if ($this->_pimpHelper->isPimpServiceItem($item)) {
                return $this->_pimpItem->getItemData($item);
            }


        return $proceed($item);
    }

}
