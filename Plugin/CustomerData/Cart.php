<?php

namespace Shirtplatform\Pimp\Plugin\CustomerData;

use Magento\Tax\Model\Config;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Checkout\Model\Session;
use Shirtplatform\Pimp\Helper\Data;
use Magento\Quote\Model\Quote;
use Magento\Checkout\CustomerData\Cart as Subject;

class Cart {

    /**
     * @var Session
     */
    private $_checkoutSession;

    /**
     * @var Data
     */
    private $_pimpHelper;

    /**
     * Array mapping productId to their original visibility
     * 
     * @var array
     */
    private $_productVisibility = [];

    /**
     * @var Quote
     */
    private $_quote;

    /**
     * @var Config
     */
    private $_taxConfig;

    /**
     * 
     * @param Session $checkoutSession
     * @param Data $pimpHelper
     * @param Config $taxConfig
     */
    public function __construct(
        Session $checkoutSession,
        Data $pimpHelper,
        Config $taxConfig
    ) {
        $this->_checkoutSession = $checkoutSession;
        $this->_pimpHelper = $pimpHelper;
        $this->_taxConfig = $taxConfig;
    }

    /**
     * Get active quote
     *
     * @access private
     * @return Quote
     */
    private function _getQuote() {
        if ($this->_quote === null) {
            $this->_quote = $this->_checkoutSession->getQuote();
        }
        return $this->_quote;
    }

    /**
     * Set visibility of pimp products to visible so that they are actually added
     * to local storage
     * 
     * @access public
     * @param Subject $subject
     * @return array
     */
    public function beforeGetSectionData($subject) {        
        foreach ($this->_getQuote()->getAllVisibleItems() as $item) {
            $product = $item->getProduct();

            if ($this->_pimpHelper->isPimpServiceProduct($product)) {
                $this->_productVisibility[$product->getId()] = $product->getVisibility();
                $product->setVisibility(Visibility::VISIBILITY_BOTH);                
            }
        }

        return [];
    }

    /**
     * Set visibility of pimp products to their original values
     * 
     * @access public
     * @param Subject $subject
     * @param array $result
     * @return array
     */
    public function afterGetSectionData($subject,
                                        $result) {        
        $quote = $this->_getQuote();
        foreach ($quote->getAllVisibleItems() as $item) {
            $product = $item->getProduct();

            if (isset($this->_productVisibility[$product->getId()])) {
                $product->setVisibility($this->_productVisibility[$product->getId()]);
            }
        }

        // $store = $quote->getStore();
        // if ($this->_taxConfig->displayCartSubtotalBoth($store) || $this->_taxConfig->displayCartSubtotalInclTax($store)) {
            // $result['subtotalAmount'] = $quote->getSubtotal();
        // }

        return $result;
    }

}
