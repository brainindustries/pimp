<?php

namespace Shirtplatform\Pimp\Setup\Patch\Data;

use Magento\Framework\App\State;
use Magento\Eav\Api\Data\AttributeSetInterfaceFactory;
use Magento\Eav\Api\AttributeSetManagementInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\TranslateInterface;
use Magento\Store\Api\StoreRepositoryInterface;
use Shirtplatform\Core\Model\ProductMaker;
use Shirtplatform\Pimp\Model\Config\Source\ServiceCodeLabels;

class CreateRemoveBackgroundProduct implements DataPatchInterface
{
    /**
     * @var State
     */
    private $_appState;

    /**
     * @var AttributeSetInterfaceFactory
     */
    private $_attributeSetFactory;

    /**
     * @var AttributeSetManagementInterface
     */
    private $_attributeSetManagement;    

    /**
     * @var EavSetupFactory
     */
    private $_eavSetup;    

    /**
     * @var \Shirtplatform\Pimp\Helper\Data
     */
    private $_helper;

    /**
     * @var ResolverInterface
     */
    private $_localeResolver;

    /**
     * @var ModuleDataSetup
     */
    private $_moduleDataSetup;    

    /**
     * @var \Magento\Catalog\Model\Product\Action
     */
    private $_productAction;

    /**
     * @var ProductMaker
     */
    private $_productMaker;    

    /**
     * @var ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     * @var \Magento\Eav\Setup\EavSetup
     */
    private $_setupInstance;

    /**
     * @var StoreRepositoryInterface
     */
    private $_storeRepository;

    /**
     * @var TranslateInterface
     */
    private $_translate;

    /**
     * @param State $appState
     * @param AttributeSetInterfaceFactory $attributeSetFactory
     * @param AttributeSetManagementInterface $attributeSetManagement     
     * @param EavSetupFactory $eavSetupFactory     
     * @param \Shirtplatform\Pimp\Helper\Data $helper
     * @param ResolverInterface $localeResolver     
     * @param ModuleDataSetupInterface $moduleDataSetup     
     * @param \Magento\Catalog\Model\Product\Action $productAction
     * @param ProductMaker $productMaker
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreRepositoryInterface $storeRepository
     * @param TranslateInterface $translate
     */
    public function __construct(
        State $appState,
        AttributeSetInterfaceFactory $attributeSetFactory,
        AttributeSetManagementInterface $attributeSetManagement,        
        EavSetupFactory $eavSetup,           
        \Shirtplatform\Pimp\Helper\Data $helper,
        ResolverInterface $localeResolver,
        ModuleDataSetupInterface $moduleDataSetup,        
        \Magento\Catalog\Model\Product\Action $productAction,
        ProductMaker $productMaker,
        ScopeConfigInterface $scopeConfig,
        StoreRepositoryInterface $storeRepository,
        TranslateInterface $translate
    ) {
        $this->_appState = $appState;
        $this->_attributeSetFactory = $attributeSetFactory;
        $this->_attributeSetManagement = $attributeSetManagement;        
        $this->_eavSetup = $eavSetup;        
        $this->_helper = $helper;
        $this->_localeResolver = $localeResolver;
        $this->_moduleDataSetup = $moduleDataSetup;        
        $this->_productAction = $productAction;
        $this->_productMaker = $productMaker;
        $this->_scopeConfig = $scopeConfig;
        $this->_storeRepository = $storeRepository;
        $this->_translate = $translate;
    }

    /**
     * Create "PIMP Service" attribute set, pimp_service_code attribute, and "Remove Background" product
     * 
     * @access public
     * @return void
     */
    public function apply()
    {
        $this->_moduleDataSetup->getConnection()->startSetup();
        $this->_setupInstance = $this->_eavSetup->create(['setup' => $this->_moduleDataSetup]);
        $this->_createAttributeSetIfNotExists();
        $this->_createPimpServiceCodeAttribute();        
        $this->_createProduct();
        $this->_moduleDataSetup->getConnection()->endSetup();
    }
    
    /**
     * Create "PIMP Service" attribute set if it doesn't exist
     * 
     * @access private
     * @return void
     */
    private function _createAttributeSetIfNotExists()
    {
        $entityTypeId = $this->_setupInstance->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $defaultAttributeSetId = $this->_setupInstance->getDefaultAttributeSetId($entityTypeId);
        $pimpAttributeSet = $this->_setupInstance->getAttributeSet($entityTypeId, \Shirtplatform\Pimp\Helper\Data::PIMPSERVICE_ATTRIBUTESET_NAME);

        if (!$pimpAttributeSet) {
            $attributeSet = $this->_attributeSetFactory->create();
            $attributeSet->setData([
                'attribute_set_name' => \Shirtplatform\Pimp\Helper\Data::PIMPSERVICE_ATTRIBUTESET_NAME,
                'entity_type_id' => $entityTypeId,
            ]);
            $this->_attributeSetManagement->create(\Magento\Catalog\Model\Product::ENTITY, $attributeSet, $defaultAttributeSetId);
        }                        
    }    

    /**
     * Create pimp_service_code attribute and assign it to "PIMP Service" attribute set
     * 
     * @access private
     * @return void
     */
    private function _createPimpServiceCodeAttribute()
    {        
        $attributeData = [
            'type' => 'varchar',
            'label' => 'PIMP Service Code',
            'input' => 'select',
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'visible' => true,
            'required' => true,
            'user_defined' => true,
            'searchable' => false,
            'filterable' => false,
            'comparable' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => false,
            'unique' => false,
            'option' => [
                'value' => [
                    'PIMP_REMOVE_BACKGROUND' => [ServiceCodeLabels::$codeLabels['PIMP_REMOVE_BACKGROUND']]
                ]
            ]
        ];
        $this->_setupInstance->addAttribute(\Magento\Catalog\Model\Product::ENTITY, 'pimp_service_code', $attributeData);
        
        //add attribute to set
        $this->_setupInstance->addAttributeToSet(\Magento\Catalog\Model\Product::ENTITY, \Shirtplatform\Pimp\Helper\Data::PIMPSERVICE_ATTRIBUTESET_NAME, 'General', 'pimp_service_code');
    }

    /**
     * Create "Remove Background" product with localized names
     * 
     * @access private
     * @return void
     */
    private function _createProduct()
    {
        $removeBackgroundData = $this->_helper->getRemoveBackgroundData();        
        $product = $this->_appState->emulateAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML, [$this->_productMaker, 'createProduct'], ['data' => $removeBackgroundData]);        

        $origLocaleCode = $this->_translate->getLocale();
        $stores = $this->_storeRepository->getList();                

        foreach ($stores as $_store) {
            if ($_store->getCode() != 'admin') {
                $newLocaleCode = $this->_scopeConfig->getValue($this->_localeResolver->getDefaultLocalePath(), \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $_store->getId());
                $this->_translate->setLocale($newLocaleCode);                
                $this->_appState->emulateAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND, [$this->_translate, 'loadData'], ['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'forceReload' => true]);
                $data = $this->_translate->getData();                
                $productName = $data['Remove Background'] ?? 'Remove Background';                
                $this->_productAction->updateAttributes([$product->getId()], ['name' => $productName], $_store->getId());                
            }
        }
        
        $this->_translate->setLocale($origLocaleCode);
        $this->_appState->emulateAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND, [$this->_translate, 'loadData'], ['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'forceReload' => true]);        
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}